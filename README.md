# README #

以下のサンプルソース
サーバー側:.NET MVC Core2.0  
クライアント側:WinForm, HttpClient, JSON.NET

# クライアント側 #
        /// <summary>
        /// JSON.NETサンプル 検索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            // http get
            var searchResult = searchCustomer(5);

            // JSON→オブジェクト変換
            var customer = JsonConvert.DeserializeObject<Customer>(searchResult.Result);

            var val1 = customer.CustomerId;
            var nestVal1 = customer.Profile.Birthplace;
            var listVal1 = customer.Profile.Activities[0].Title;

        }

        /// <summary>
        /// JSON.NETサンプル(POST)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            // http post
            var postResult = postCustomer();
        }


        /// <summary>
        /// HTTP GET
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private async Task<string> searchCustomer(int id)
        {
            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri("http://localhost:59864/api/Customer/5")
            };

            var httpResponseMessage = _httpClient.SendAsync(httpRequestMessage);

            return await httpResponseMessage.Result.Content.ReadAsStringAsync();
        }

        /// <summary>
        /// HTTP POST
        /// </summary>
        /// <returns></returns>
        private async Task<string> postCustomer()
        {
            var searchResult = searchCustomer(5);

            var customer = JsonConvert.DeserializeObject<Customer>(searchResult.Result);

            customer.Mail = "fugafuga@fuga.com";

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri("http://localhost:59864/api/Customer"),
                Content = new StringContent(JsonConvert.SerializeObject(customer), Encoding.UTF8, "application/json")
            };

            var httpResponseMessage = _httpClient.SendAsync(httpRequestMessage);

            return await httpResponseMessage.Result.Content.ReadAsStringAsync();
        }
        
# Web API #
        // GET: api/Customer/5
        [HttpGet("{id}", Name = "Get")]
        public Customer Get(int id)
        {
            Customer customer = new Customer();
            customer.CustomerId = "ID001";
            customer.Name = "テスト太郎";
            customer.Mail = "hogehoge@hoge.com";

            Profile profile = new Profile();
            profile.Birthplace = "青森県";

            MyActivity act1 = new MyActivity();
            act1.Title = "あいうえお";
            act1.Memo = "１１１１";

            MyActivity act2 = new MyActivity();
            act2.Title = "かきくけこ";
            act2.Memo = "２２２２";

            List<MyActivity> activities = new List<MyActivity>();
            activities.Add(act1);
            activities.Add(act2);

            profile.Activities = activities;

            customer.Profile = profile;

            return customer;
        }
結果のJSON
`{"customerId":"ID001","name":"テスト太郎","mail":"hogehoge@hoge.com","profile":{"birthplace":"青森県","activities":[{"title":"あいうえお","memo":"１１１１"},{"title":"かきくけこ","memo":"２２２２"}]}}`

POSTされたJSONのモデルバインディング  

        // POST: api/Customer
        [HttpPost]
        public string Post([FromBody]Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return "failed";
            }

            return "success!! : " + customer.Mail;
        }

[FromBody]属性により、requestのbodyをオブジェクトにバインドしてくれる

# クライアント側(Dynamic.JSON)
        /// <summary>
        /// DynamicJSONサンプル
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            // http get
            var searchResult = searchCustomer(5);

            // JSON→オブジェクト変換
            var customer = DynamicJson.Parse(searchResult.Result, Encoding.UTF8);

            // 型関係なく動的にアクセスできる (javascriptでjson操作するイメージに近い)
            var val1 = customer.customerId;
            var nestVal1 = customer.profile.birthplace;
            var listVal1 = customer.profile.activities[0].title;

            string loopVal = "";
            foreach (var li in customer.profile.activities)
            {
                loopVal += li.title;
            }
        }