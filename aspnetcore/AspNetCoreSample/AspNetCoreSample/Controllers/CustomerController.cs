﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspNetCoreSample.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AspNetCoreSample.Controllers
{
    [Produces("application/json")]
    [Route("api/Customer")]
    public class CustomerController : Controller
    {
        // GET: api/Customer
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Customer/5
        [HttpGet("{id}", Name = "Get")]
        public Customer Get(int id)
        {
            Customer customer = new Customer();
            customer.CustomerId = "ID001";
            customer.Name = "テスト太郎";
            customer.Mail = "hogehoge@hoge.com";

            Profile profile = new Profile();
            profile.Birthplace = "青森県";

            MyActivity act1 = new MyActivity();
            act1.Title = "あいうえお";
            act1.Memo = "１１１１";

            MyActivity act2 = new MyActivity();
            act2.Title = "かきくけこ";
            act2.Memo = "２２２２";

            List<MyActivity> activities = new List<MyActivity>();
            activities.Add(act1);
            activities.Add(act2);

            profile.Activities = activities;

            customer.Profile = profile;

            return customer;
        }

        // POST: api/Customer
        [HttpPost]
        public string Post([FromBody]Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return "failed";
            }

            return "success!! : " + customer.Mail;
        }

        // PUT: api/Customer/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
