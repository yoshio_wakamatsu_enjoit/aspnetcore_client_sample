﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreSample.Models
{
    public class MyActivity
    {
        public string Title { get; set; }
        public string Memo { get; set; }
    }
}
