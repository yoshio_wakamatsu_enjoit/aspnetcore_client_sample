﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspNetCoreSample.Models
{
    public class Profile
    {
        /// <summary>
        /// 出身地.
        /// </summary>
        public string Birthplace { get; set; }

        /// <summary>
        /// 活動.
        /// </summary>
        public List<MyActivity> Activities { get; set; }


    }
}
