﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspNetCoreFormClientSample.Models
{
    public class MyActivity
    {
        public string Title { get; set; }
        public string Memo { get; set; }
    }
}
