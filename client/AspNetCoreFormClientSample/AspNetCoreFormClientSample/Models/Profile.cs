﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspNetCoreFormClientSample.Models
{
    public class Profile
    {
        /// <summary>
        /// 出身地.
        /// </summary>
        public string Birthplace { get; set; }

        /// <summary>
        /// 活動.
        /// </summary>
        public List<MyActivity> Activities { get; set; }


    }
}
