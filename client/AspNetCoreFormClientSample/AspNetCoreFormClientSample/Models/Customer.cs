﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspNetCoreFormClientSample.Models
{
    public class Customer
    {
        public string CustomerId { get; set; }
        public string Name { get; set; }
        public string Mail { get; set; }

        public Profile Profile { get; set; }

        public bool IsAdmin
        {
            get { return CustomerId.Equals("ID001"); }
        }
    }
}
