﻿using AspNetCoreFormClientSample.Models;
using Codeplex.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AspNetCoreFormClientSample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        HttpClient _httpClient = new HttpClient();

        /// <summary>
        /// DynamicJSONサンプル
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            // http get
            var searchResult = searchCustomer(5);

            // JSON→オブジェクト変換
            var customer = DynamicJson.Parse(searchResult.Result, Encoding.UTF8);

            // 型関係なく動的にアクセスできる (javascriptでjson操作するイメージに近い)
            var val1 = customer.customerId;
            var nestVal1 = customer.profile.birthplace;
            var listVal1 = customer.profile.activities[0].title;

            string loopVal = "";
            foreach (var li in customer.profile.activities)
            {
                loopVal += li.title;
            }
        }

        /// <summary>
        /// JSON.NETサンプル 検索
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            // http get
            var searchResult = searchCustomer(5);

            // JSON→オブジェクト変換
            var customer = JsonConvert.DeserializeObject<Customer>(searchResult.Result);

            var val1 = customer.CustomerId;
            var nestVal1 = customer.Profile.Birthplace;
            var listVal1 = customer.Profile.Activities[0].Title;

        }

        /// <summary>
        /// JSON.NETサンプル(POST)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            // http post
            var postResult = postCustomer();
        }


        /// <summary>
        /// HTTP GET
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private async Task<string> searchCustomer(int id)
        {
            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri("http://localhost:56569/api/Customer/5")
            };

            var httpResponseMessage = _httpClient.SendAsync(httpRequestMessage);

            return await httpResponseMessage.Result.Content.ReadAsStringAsync();
        }

        /// <summary>
        /// HTTP POST
        /// </summary>
        /// <returns></returns>
        private async Task<string> postCustomer()
        {
            var searchResult = searchCustomer(5);

            var customer = JsonConvert.DeserializeObject<Customer>(searchResult.Result);

            customer.Mail = "fugafuga@fuga.com";

            var httpRequestMessage = new HttpRequestMessage
            {
                Method = HttpMethod.Post,
                RequestUri = new Uri("http://localhost:56569/api/Customer"),
                Content = new StringContent(JsonConvert.SerializeObject(customer), Encoding.UTF8, "application/json")
            };

            var httpResponseMessage = _httpClient.SendAsync(httpRequestMessage);

            return await httpResponseMessage.Result.Content.ReadAsStringAsync();
        }
    }
}
